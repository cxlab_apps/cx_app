import zipfile
import os
import sys
import requests
import time
import xmltodict
import json
  
args = sys.argv
requests_rate = 10 #seconds

server = args[1]
project_name = args[2]
team_name = args[3]
cxUsername = str(args[4])
cxPassword = str(args[5])
gitRepo = str(args[6])
gitBranch = str(args[7])

clientSecret = "014DF517-39D1-4453-B7B3-9930C563627C"
preset_name = ""
folder_exclusions = ""
file_exclusions = ""
script_name = "GitLab CI"
  
endpoint_server = server + "/cxrestapi/"

def get_oauth2_token():
    oauth2_data = {
        "username": cxUsername,
        "password": cxPassword,
        "grant_type": "password",
        "scope": "sast_rest_api",
        "client_id": "resource_owner_client",
        "client_secret": clientSecret
    }

    oauth2_response = requests.post(endpoint_server + "/auth/identity/connect/token", data=oauth2_data)
    if oauth2_response.status_code == 200:
        json = oauth2_response.json()
        return json["token_type"] + " " + json["access_token"]
    else:
       print (json.dumps(oauth2_response.json()))

       exit(0)
  
token = get_oauth2_token()
headers = {
    "Authorization": token
}
  
  
def print_status(status_name, project_id, scan_id, report_id=None):
    if report_id:
        print(status_name + " - Project - " + str(project_id) + " - Scan ID - " + scan_id + " - Report ID - " +
              report_id)
    else:
        print(status_name + " - Project - " + str(project_id) + " - Scan ID - " + scan_id)


def error(resp):
    print("Error - " + str(resp.status_code) + " :\n" + resp.text)
    return None


def get_team_by_name(team_name):
    teams_response = requests.get(endpoint_server + "/auth/teams", headers=headers)
    if teams_response.status_code == 200:
        teams = teams_response.json()
        for team in teams:
            if team['fullName'] == team_name:
                return team['id']
        return []
    else:
        error(teams_response)
        return None


def create_project(project_name, team_id):
    create_project_data = {
        "name": project_name,
        "owningTeam": team_id,
        "isPublic": True
    }
    project_response = requests.post(endpoint_server + "/projects", headers=headers, data=create_project_data)
    if project_response.status_code == 201:
        return project_response.json()
    else:
        return False
 
     
def zipfolder(foldername, target_dir):           
    zipobj = zipfile.ZipFile(foldername + '.zip', 'w', zipfile.ZIP_DEFLATED)
    rootlen = len(target_dir) + 1
    for base, dirs, files in os.walk(target_dir):
        for file in files:
            fn = os.path.join(base, file)
            zipobj.write(fn, fn[rootlen:])

def set_git_project(project_id, gitRepo, gitBranch):
    headersGitProject = {
        "Authorization": token,
        "cxOrigin": script_name
    }
    git_data = {
        "url": gitRepo,
        "branch": gitBranch
    }
    
    print (gitRepo + ":" + gitBranch)

    set_git_project_response = requests.post(
        endpoint_server + "/projects/" + str(project_id) + "/sourceCode/remoteSettings/git", headers=headersGitProject, data=git_data)
    if set_git_project_response.status_code == 204:
        return True
    else:
        error(set_git_project_response)
        return False


def set_project_exclude_settings(project_id, exclude_folders, exclude_files):

    exclude_settings_data = {
        "excludeFoldersPattern": exclude_folders,
        "excludeFilesPattern": exclude_files
    }
    exclude_settings_response = requests.put(
        endpoint_server + "/projects/" + str(project_id) + "/sourceCode/excludeSettings", headers=headers,
        data=exclude_settings_data)
    if exclude_settings_response.status_code == 200:
        return True
    else:
        error(exclude_settings_response)
        return False


def get_projects_by_name(project_name):

    projects_response = requests.get(endpoint_server + "/projects", headers=headers)
    if projects_response.status_code == 200:
        projects = projects_response.json()
        for proj in projects:
            if proj['name'] == project_name:
                return proj['id']
        return False
    else:
        error(projects_response)
        return False
  
  
def get_preset_by_name(preset_name):

    if preset_name == "":
        preset_name = "Checkmarx Default"

    presets_response = requests.get(endpoint_server + "/sast/presets", headers=headers)
    if presets_response.status_code == 200:
        presets = presets_response.json()
        for preset in presets:
            if preset['name'] == preset_name:
                return preset['id']
        return []
    else:
        error(presets_response)
        return False


def get_engine_server():

    engine_response = requests.get(endpoint_server + "/sast/engineServers", headers=headers)
    if engine_response.status_code == 200:
        engines = engine_response.json()
        return engines[0]['id']
    else:
        error(engine_response)
        return False


def update_project_configuration(project_id, preset_id, engine_id):

    project_config_data = {
        "projectId": project_id,
        "presetId": preset_id,
        "engineConfigurationId": engine_id
    }
    project_config_response = requests.post(endpoint_server + "/sast/scanSettings", headers=headers,
                                            data=project_config_data)
    if project_config_response.status_code == 200:
        return True
    else:
        error(project_config_response)
        return False


def scan_project(project_id, project_name):

    data = {
        "projectId": project_id,
        "isIncremental": False,
        "isPublic": True,
        "forceScan": True
    }
    headersScanProject = {
        "Authorization": token,
        "cxOrigin": script_name
    }
    start_scan_response = requests.post(endpoint_server + "/sast/scans", headers=headersScanProject, data=data)
    if start_scan_response.status_code == 201:
        scan = start_scan_response.json()
        scan_id = str(scan["id"])
        status = "New"
        print_status(status, project_name, scan_id)
        past_status = status
        while status != "Finished":
            time.sleep(requests_rate)
            get_scan_response = requests.get(endpoint_server + scan["link"]["uri"], headers=headers)
            if get_scan_response.status_code == 200:
                status = get_scan_response.json()["status"]["name"]
                if past_status != status:
                    print_status(status, project_name, scan_id)
                    past_status = status
            else:
                return error(get_scan_response)
        print_status("Scan Finished", project_name, scan_id)
        return scan_id
    else:
        return error(start_scan_response)
  
  
def generate_report(scan_id, project_name):

    report_request_data = {
        "reportType": "XML",
        "scanId": scan_id
    }
    new_scan_report_response = requests.post(endpoint_server + "/reports/sastScan", headers=headers,
                                             data=report_request_data)
    if new_scan_report_response.status_code == 202:
        report = new_scan_report_response.json()
        report_id = str(report["reportId"])
        status = "InProcess"
        past_status = status
        print_status(status, project_name, scan_id, report_id)
        while status != "Created":
            time.sleep(requests_rate)
            get_report_status_response = requests.get(endpoint_server + report["links"]["status"]["uri"],
                                                      headers=headers)
            if get_report_status_response.status_code == 200:
                status = get_report_status_response.json()["status"]["value"]
                if past_status != status:
                    print_status(status, project_name, scan_id, report_id)
                    past_status = status
            else:
                return error(get_report_status_response)
        print("Report Generated - " + report_id)
        get_report_response = requests.get(endpoint_server + report["links"]["report"]["uri"],
                                           headers=headers)
        if get_report_response.status_code == 200:
            return get_report_response.text
        else:
            return error(get_report_response)
    else:
        return error(new_scan_report_response)

  
def create_json_file(output):

    file = open("gl-sast-report.json","w+")

    file.write(output)

    file.close()

  
def parse_xml(doc):
    sastReport = {}
    vulnList = []

    sastReport['version'] = "2.0"

    if doc and 'CxXMLResults' in doc:
        xml_results = doc['CxXMLResults']

        if xml_results and 'Query' in xml_results:
            for query in xml_results['Query']:
                results = query['Result']
                list_results = []
                if isinstance(results, list):
                    list_results = results
                else:
                    list_results.append(results)
                for result in list_results:
                    vulnElement = {}

                    vulnElement['category'] = "sast"
                    vulnElement['name'] = query["@name"]
                    vulnElement['message'] = query["@name"] + " found in code."
                    vulnElement['description'] = query["@name"]
                    vulnElement['cve'] = query["@cweId"]
                    vulnElement['severity'] = query["@Severity"]
                    vulnElement['confidence'] = query["@Severity"]
                    vulnElement['solution'] = "Review " + result["@DeepLink"] + " for details."

                    scanner = {}
                    scanner['id'] = "Checkmarx"
                    scanner['name'] = "Checkmarx"

                    vulnElement['scanner'] = scanner

                    location = {}
                    location['file'] = result["@FileName"]
                    location['start_line'] = result["@Line"]
                    location['end_line'] = "0"
                    location['class'] = "N/A"
                    location['method'] = "N/A"

                    dependency = {}
                    package = {}

                    dependency['package'] = package
                    location['dependency'] = dependency

                    vulnElement['location'] = location

                    identifiersList = []

                    identifiers= {}
                    identifiers['type'] = "checkmarx_bug"
                    identifiers['name'] = "Checkmarx - " + query["@name"]
                    identifiers['value'] = query["@name"]
                    identifiers['url'] = result["@DeepLink"]

                    identifiersList.append(identifiers)

                    identifiers= {}
                    identifiers['type'] = "cwe"
                    identifiers['name'] = query["@cweId"]
                    identifiers['value'] = "0"
                    identifiers['url'] = "https://cwe.mitre.org"

                    identifiersList.append(identifiers)

                    vulnElement['identifiers'] = identifiersList

                    vulnList.append(vulnElement)

    sastReport['vulnerabilities'] = vulnList

    remediations = []
    sastReport['remediations'] = remediations 

    return (json.dumps(sastReport))

#-----------------------------------------------------------------------------------------------------

project_id = "0"
team_id = get_team_by_name(team_name)

if team_id:
    project_was_created = create_project(project_name, team_id)
    if project_was_created:
        print("Project Created")
        project_id = str(project_was_created['id'])
    else:
        print("Project Already Exists")
        project_id = str(get_projects_by_name(project_name))
  
    print("Project : " + project_name + " - " + project_id)
    source_code_updated = set_git_project(project_id, gitRepo, gitBranch)
    if source_code_updated:
        print("Source Code Updated")
    else:
        print("Source Code Error")
  
    exclude_settings_updated = set_project_exclude_settings(project_id, folder_exclusions, file_exclusions)
    if exclude_settings_updated:
        print("Exclude Setting Updated")
    else:
        print("Exclude Setting Error")
  
    preset_id = str(get_preset_by_name(preset_name))
  
    engine_id = str(get_engine_server())
  
    project_updated = update_project_configuration(project_id, preset_id, engine_id)
    if project_updated:
        print("Project Configuration Updated")
    else:
        print("Project Configuration Error")
  
    scan_id = scan_project(project_id, project_name)
  
    if scan_id:
        xml = generate_report(scan_id, project_name)
      
        if xml:
            document = xmltodict.parse(xml)
            json_output = parse_xml(document)
            create_json_file (json_output)
        else:
            print("Error retrieving the XML Results")
            exit(2)
    else:
        print("Invalid Scan Id - " + str(scan_id))
        exit(2)
else:
    print("Invalid Team Name")
    print(team_name)
    exit(2)
