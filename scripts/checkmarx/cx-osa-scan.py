import zipfile
import os
import sys
import requests
import time
import xmltodict
import json

args = sys.argv
requests_rate = 10 #seconds

server = args[1]
project_name = args[2]
team_name = args[3]
cxUsername = str(args[4])
cxPassword = str(args[5])
zipfile = str(args[6])

clientSecret = "014DF517-39D1-4453-B7B3-9930C563627C"
preset_name = ""
folder_exclusions = ""
file_exclusions = ""
filePath = ""
script_name = "GitLab CI"
  
endpoint_server = server + "/cxrestapi"

def get_oauth2_token():
    oauth2_data = {
        "username": cxUsername,
        "password": cxPassword,
        "grant_type": "password",
        "scope": "sast_rest_api",
        "client_id": "resource_owner_client",
        "client_secret": clientSecret
    }
    oauth2_response = requests.post(endpoint_server + "/auth/identity/connect/token", data=oauth2_data)
    if oauth2_response.status_code == 200:
        json = oauth2_response.json()
        return json["token_type"] + " " + json["access_token"]
    else:
       print ("error in oath2_token")
       exit(0)
  
token = get_oauth2_token()
headers = {
    "Authorization": token
}
  
  
def print_status(status_name, project_id, scan_id, report_id=None):
    if report_id:
        print(status_name + " - Project - " + str(project_id) + " - Scan ID - " + scan_id + " - Report ID - " +
              report_id)
    else:
        print(status_name + " - Project - " + str(project_id) + " - Scan ID - " + scan_id)


def error(resp):
    print("Error - " + str(resp.status_code) + " :\n" + resp.text)
    return None


def get_team_by_name(team_name):
    teams_response = requests.get(endpoint_server + "/auth/teams", headers=headers)
    if teams_response.status_code == 200:
        teams = teams_response.json()
        for team in teams:
            if team['fullName'] == team_name:
                return team['id']
        return []
    else:
        error(teams_response)
        return None


def create_project(project_name, team_id):
    create_project_data = {
        "name": project_name,
        "owningTeam": team_id,
        "isPublic": True
    }
    project_response = requests.post(endpoint_server + "/projects", headers=headers, data=create_project_data)
    if project_response.status_code == 201:
        return project_response.json()
    else:
        return False
 

def get_projects_by_name(project_name):

    projects_response = requests.get(endpoint_server + "/projects", headers=headers)
    if projects_response.status_code == 200:
        projects = projects_response.json()
        for proj in projects:
            if proj['name'] == project_name:
                return proj['id']
        return False
    else:
        error(projects_response)
        return False

def scan_project(project_id, project_name, zippedFile):

    data = {
        "projectId": project_id
    }
   
    file = {
        "zippedSource": open(zippedFile,'rb')
    }
    
    start_scan_response = requests.post(endpoint_server + "/osa/scans", data=data, headers=headers, files=file)

    scan_id = start_scan_response.json()['scanId']

    data = {
        "scanId": str(scan_id)
    }
    
    response = requests.get(endpoint_server + "/osa/scans/" + str(scan_id), data=data, headers=headers)
    if response.status_code == 200:
        status = response.json()['findingsStatus']
        while status != "FINISHED":
            time.sleep(10)
            response = requests.get(endpoint_server + "/osa/scans/" + str(scan_id), data=data, headers=headers)
            if response.status_code == 200:
                status = response.json()['findingsStatus']
            else:
                sys.exit()
    else:
        sys.exit()

    return scan_id

def generate_report(scan_id, project_name):

    data = {
        "scanId": scan_id
    }

    # JPB - this was frustrating, not sure why data=data doesn't work here.  had to use ?scanId=

    vuln_response = requests.get(endpoint_server + "/osa/vulnerabilities?scanId=" + scan_id, headers=headers)
    
    if vuln_response.status_code == 200:
        output = vuln_response.json()

        return (output)
    else:
        print ("error getting vulns: " + str (vuln_response.status_code))
        sys.exit()

  
def create_json_file(output):

    file = open("gl-dependency-scanning-report.json","w+")

    file.write(output)

    file.close()

  
def convert_json(output):
    dependReport = {}
    vulnList = []


    for result in output:
        vulnElement = {}

        vulnElement['tool'] = "CxOSA"
        vulnElement['message'] = result['description']
        vulnElement['url'] = result['url']
        vulnElement['cve'] = result['cveName']
        vulnElement['file'] = result['sourceFileName']
        vulnElement['priority'] = result['severity']['name']
                    
        vulnList.append(vulnElement)

    return (json.dumps(vulnList))

#-----------------------------------------------------------------------------------------------------

project_id = "0"
team_id = get_team_by_name(team_name)

# 1. POST /projects
# 2. POST /osa/scans - https://checkmarx.atlassian.net/wiki/spaces/CCOD/pages/991461817/Create+an+OSA+Scan+Request+-+POST+osa+scans+v8.8.0+and+up
# 3. GET /osa/vulnerabilties - https://checkmarx.atlassian.net/wiki/spaces/CCOD/pages/1183842601/Get+OSA+Scan+Vulnerabilities+by+Id+-+GET+osa+vulnerabilities+v8.9.0+and+up

if team_id:
    project_was_created = create_project(project_name, team_id)
    if project_was_created:
        print("Project Created")
        project_id = str(project_was_created['id'])
    else:
        print("Project Already Exists")
        project_id = str(get_projects_by_name(project_name))
  
    print("Project : " + project_name + " - " + project_id)

  
    # 2 POST /osa/scans

    scan_id = scan_project(project_id, project_name, zipfile)
  
    # 3. GET /osa/vulnerabilities

    if scan_id:
        vulnerabilties = generate_report(scan_id, project_name)

        out = convert_json(vulnerabilties)

        create_json_file(out)
        
else:
    print("Invalid Team Name")
    print(team_name)
    exit(2)
